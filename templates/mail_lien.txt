Cette année encore, la fin de l'année approchant, le Bureau du BdE se prépare à passer la main et partir à la retraite après 1 an de mandat.

Afin de renouveler le collège des membres adhérents du BdE INSA Lyon (Bureau), les élections sont organisées par voie électronique.
En tant que membre adhérent au Bureau des Élèves (car vous avez une carte VA), vous êtes convié.e.s à participer à l'élection du nouveau Bureau.
Votez en ligne avec le lien suivant:

{{link}}

Cordialement,
le BdE
