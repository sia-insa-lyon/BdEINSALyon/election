#!/bin/bash
cd /app
/app/manage.py migrate
uwsgi --module=voteva.wsgi:application --http=0.0.0.0:8000 --threads=4 --processes=2 \
  --env DJANGO_SETTINGS_MODULE=voteva.settings