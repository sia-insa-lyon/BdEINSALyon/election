from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms
from django.urls import reverse
from django.utils.timezone import now
from tempus_dominus.widgets import DateTimePicker

from web import models


class BulletinForm(forms.ModelForm):
    def __init__(self, token, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('vote', kwargs={'token': token})
        self.helper.add_input(Submit('submit', 'Voter'))

    class Meta:
        model = models.Vote
        exclude = []

class SettingsForm(forms.ModelForm):
    class Meta:
        model = models.ElectionSettings
        exclude = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = reverse('settings')
        self.helper.add_input(Submit('submit', 'Enregistrer'))

    start_datetime = forms.DateTimeField(label="Début du suffrage", initial=now, widget=DateTimePicker())
    stop_datetime = forms.DateTimeField(label="Fin du suffrage", initial=now, widget=DateTimePicker())
