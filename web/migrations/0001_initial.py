# Generated by Django 3.0.6 on 2020-05-21 14:46

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ElectionSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('enabled', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Liste',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=1023, verbose_name='Nom')),
            ],
            options={
                'verbose_name': 'Liste',
            },
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code_va', models.CharField(max_length=13, unique=True, verbose_name='N° de carte VA')),
                ('liste',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='votes', to='web.Liste',
                                   verbose_name='Liste')),
            ],
        ),
    ]
