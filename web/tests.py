# Create your tests here.
import datetime

from django.contrib.auth import get_user_model
from django.test import Client, SimpleTestCase
from django.utils.timezone import now

from web.models import Liste, ElectionSettings, Votant

User = get_user_model()

class TestSuffrage(SimpleTestCase):
    databases = ['default']

    def setUp(self) -> None:
        ElectionSettings.objects.create(enabled=True, start_datetime=now(),
                                        stop_datetime=datetime.datetime(year=2030, month=1, day=3, hour=23, minute=57,
                                                                        second=33))
        Liste.objects.create(name="Buroceans'9")
        Liste.objects.create(name='Annuliste')
        self.token = Votant.objects.create(email=str(Votant.objects.count()) + "test@yopmail.com",
                                           email_sent=False).token.__str__()
        self.client = Client()

    def test_getform(self):
        r = self.client.get('/vote/' + self.token + '/')
        self.assertEqual(r.status_code, 200)

    def test_vote_corect(self):
        reponse = self.client.post('/vote/' + self.token + '/', {
            'liste': "2",
        })
        # print(f"s{reponse.status_code} c {reponse.content.decode('utf-8')}")
        self.assertEqual(reponse.status_code, 302)
        self.assertEqual(reponse.url, "/voteok/")
        self.assertRedirects(reponse, '/voteok/')

    def test_vote_deuxfois(self):
        """
        si on essaie de voter deux fois
        """
        reponse1 = self.client.post('/vote/' + self.token + '/', {
            'liste': "2",
        })  # la DB se garde entre les tests visiblement
        self.assertRedirects(reponse1, '/voteok/')
        reponse2 = self.client.post('/vote/' + self.token + '/', {
            'liste': "2",
        })
        self.assertNotEqual(reponse2.status_code, 302)

    def test_send_tokenemails(self):
        admin = User.objects.create(username='r00t')
        admin.set_password('yoloyolo')
        admin.save()
        self.client.login(username='r00t', password='yoloyolo')
        for _ in range(5):
            Votant.objects.create(email=str(Votant.objects.count()) + "test@yopmail.com", email_sent=False)
        rep = self.client.get(path='/send_emails/')
        self.assertEqual(rep.status_code, 201)
