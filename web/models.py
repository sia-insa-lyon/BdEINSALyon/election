import uuid

from django.db import models
from django.utils.timezone import now


class ElectionSettings(models.Model):
    enabled = models.BooleanField(default=True, verbose_name="Actif")
    start_datetime = models.DateTimeField(verbose_name="Début du suffrage", blank=False, null=False, default=now)
    stop_datetime = models.DateTimeField(verbose_name="Fin du suffrage", blank=False, null=False, default=now)

    @property
    def is_open(self):
        return self.enabled and self.start_datetime <= now() and self.stop_datetime >= now()


class Liste(models.Model):
    class Meta:
        verbose_name = "Liste"
        ordering = ("name",)

    name = models.CharField(verbose_name="Nom", null=False, blank=False, max_length=1023)

    def __str__(self):
        return self.name


class Votant(models.Model):
    email = models.EmailField(blank=False, null=False, verbose_name="Email", unique=True)
    token = models.UUIDField(primary_key=True, default=uuid.uuid4)
    a_vote = models.BooleanField(default=False)
    email_sent = models.BooleanField(default=False)

    def __str__(self):
        return self.email


class Vote(models.Model):
    liste = models.ForeignKey(to=Liste, verbose_name="Liste", on_delete=models.CASCADE, related_name='votes')

    def __str__(self):
        return f"vote pour {self.liste.name}"
