from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.mail import send_mail
from django.http import HttpResponse, HttpRequest
from django.shortcuts import render
from django.template.loader import get_template
from django.urls import reverse
from django.views.generic import CreateView, TemplateView, UpdateView, ListView

from web import models, forms


# Create your views here.

def index(request):
    settings = models.ElectionSettings.objects.first()
    return render(request, template_name='listes.html', context={
        'listes': models.Liste.objects.all(),
        'settings': settings,
    })


class VoteFormView(CreateView):
    model = models.Vote
    form_class = forms.BulletinForm
    template_name = 'vote.html'

    def get_success_url(self):
        return reverse('voteok')

    def post(self, request: HttpRequest, *args, **kwargs):
        if not models.ElectionSettings.objects.first().is_open:
            return HttpResponse("les élections ne sont pas ouvertes")
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        token = self.kwargs['token']
        votant: models.Votant = models.Votant.objects.get(token=token)
        if votant.a_vote:
            return HttpResponse("vous avez deja voté lol")
        response: HttpResponse = super().form_valid(form)
        votant.a_vote = True
        votant.save()

        return response

    def get_form(self, form_class=forms.BulletinForm):
        kwargs = self.get_form_kwargs()
        kwargs['token'] = self.kwargs['token']
        return form_class(**kwargs)

    def get_context_data(self, **kwargs):
        return super().get_context_data(settings=models.ElectionSettings.objects.first(), **kwargs)


class VoteOkView(TemplateView):
    template_name = 'voteok.html'


class SettingsView(LoginRequiredMixin, UpdateView):
    form_class = forms.SettingsForm
    template_name = 'settings.html'

    def get_object(self, queryset=None):
        return models.ElectionSettings.objects.first()

    def get_success_url(self):
        return reverse('settings')


class ResultatsView(LoginRequiredMixin, ListView):
    template_name = 'resultats.html'
    model = models.Liste


@login_required()
def send_token_emails(request, *args, **kwargs):
    tosend = models.Votant.objects.filter(email_sent=False, a_vote=False)[:10]
    for votant in tosend:
        if send_mail(
                subject='Élection du collège des membres adhérents "Bureau" du BdE INSA Lyon',
                from_email=settings.FROM_EMAIL,
                recipient_list=[votant.email],
                fail_silently=False,
                message=get_template("mail_lien.txt").render(
                    {'link': f"https://{settings.HOST}/vote/{str(votant.token)}/"})
        ) == 1:
            print(f"votante: {votant}")
            votant.email_sent = True
            votant.save()
        else:
            return render(request, 'sendmail_status.html', {
                'fail': True,
                'email': votant.email,
                'remaining': models.Votant.objects.filter(email_sent=False).count(),
                'sent': models.Votant.objects.filter(email_sent=True).count()
            })
    return render(request, 'sendmail_status.html', {
        'fail': False,
        'remaining': models.Votant.objects.filter(email_sent=False).count(),
        'sent': models.Votant.objects.filter(email_sent=True).count()
    }, status=201 if models.Votant.objects.filter(email_sent=False).count() == 0 else 200)
