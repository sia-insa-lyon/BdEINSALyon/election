from django.conf import settings
from django.contrib import admin
# Register your models here.
from django.core.mail import send_mail
from django.template.loader import get_template
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from web.models import *


@admin.register(ElectionSettings)
@admin.register(Vote)
@admin.register(Liste)
class DummyAdmin(admin.ModelAdmin):
    pass


class VotantResource(resources.ModelResource):
    class Meta:
        model = Votant
        fields = ['email']
        import_id_fields = ['email']


@admin.register(Votant)
class VotantAdmin(ImportExportModelAdmin):
    resource_class = VotantResource
    search_fields = ('email',)
    list_display = ('email', 'email_sent', 'a_vote')
    actions = ('envoyer_les_mails',)

    def envoyer_les_mails(self, request, queryset):
        for obj in queryset.filter(email_sent=False):
            votant: Votant = obj
            send_mail(
                subject='Élection du collège des membres adhérents "Bureau" du BdE INSA Lyon',
                from_email=settings.FROM_EMAIL,
                recipient_list=[votant.email],
                fail_silently=False,
                message=get_template("mail_lien.txt").render(
                    {'link': f"https://{settings.HOST}/vote/{str(votant.token)}/"})
            )
            votant.email_sent = True
            votant.save()
            self.message_user(request,
                              "{} mails envoyé(s), vérifiez les logs Mailgun".format(
                                  queryset.count()))

    envoyer_les_mails.short_description = "Renvoyer les mails"
