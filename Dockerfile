FROM python:3.6

WORKDIR /app

COPY requirements.txt /app
RUN pip3 install -r requirements.txt

COPY . /app
RUN chmod +x /app/entrypoint.sh && mv /app/entrypoint.sh /entrypoint.sh

ENV SQLITE_FILE "/app/db/db.sqlite3"
ENV ADHESION_CLIENT_SECRET ""
ENV ADHESION_CLIENT_ID ""
ENV DEBUG "False"

ENV USE_POSTGRES "True"
ENV DB_HOST "db"
ENV DB_NAME "election"
ENV DB_USER "voteva"
ENV DB_PASSWORD "voteva"
ENV DB_PORT "5432"
EXPOSE 8000

CMD ["/entrypoint.sh"]
