# Application pour l'élection du Bureau du BdE INSA Lyon
Cette application est un serveur Django sans front-end.

## Utilisation
* l'utilisateur arrive sur le site
* regarde la liste des listes
* rentre des informations permettant de vérifier qu'il a bien une adhésion à la Vie Associative (n° de carte VA)
* choisit la liste pour laquelle il vote

## Fonctionnement
Avant l'enregistrement de l'objet vote, le n° de carte VA est vérifié auprès du 
serveur *Adhesion-api*.
